from django.urls import path
from .views import note_list,  note_list,add_note, index

urlpatterns = [
    path('', index, name='index'),
    path('add-note/',add_note,name='add note'),
    path('note-list/',note_list,name='note list')
]