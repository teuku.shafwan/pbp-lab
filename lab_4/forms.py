from django import forms
from django.forms import fields
from .models import Note

class Noteform(forms.ModelForm):
    class Meta:
        model = Note
        fields=[
            'To',
            'From',
            'title',
            'message'
        ]