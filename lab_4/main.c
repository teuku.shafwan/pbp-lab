/*
 *  Nama Lengkap    : Muhammad Ihsan Al Farisi
 *  NPM            : 2006596693
 *  Kelas          : SOSI - A
 */
#include <stdio.h>
#include <stdlib.h>

void main(int argc, char *argv[]) {
    char* temp;
    
    //sorting array
    for (int i = 0; i < argc; i++) {     
        for (int j = i+1; j < argc; j++) {     
           if(atoi(argv[i]) > atoi(argv[j])) {    
               temp = argv[i];    
               argv[i] = argv[j];    
               argv[j] = temp;    
           }     
        }     
    }
        
    char* res;
    int index;
    char* result[6];
    
    // insert 3 angka terkecil (dari yang terkecil)
    for (int i = 1; i < argc; i++) {
        if (index == 0) {
            res = argv[1];
            result[index++] = res;
        } else {
            int before = atoi(res);
            // cek angka sama atau tidak dengan yang sebelumnya
            if (atoi(argv[i]) == before) {
            } else {
                res = argv[i];
                result[index++] = res;
            }
        }
        if (index == 3) break;
    }
    
    index = 5;
    // insert 3 angka terbesar (dari yang terbesar)
    for (int i = argc-1; i >= 0; i--) {
        if (index == 6) {
            res = argv[argc-1];
            result[index--] = res;
        } else {
            int before = atoi(res);
            // cek angka sama atau tidak dengan yang sebelumnya
            if (atoi(argv[i]) == before) {
            } else {
                res = argv[i];
                result[index--] = res;
            }
        }
        if (index == 2) break;
    }
    
    
    for (int i = 0; i < 6; i++) {
        printf("%s ", result[i]);
    }
    printf("\n");
}
