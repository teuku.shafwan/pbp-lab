from django.shortcuts import render
from .models import Note
from .forms import Noteform
from django.http import HttpResponseRedirect
def index(request):
    note = Note.objects.all()
    response = {'Notes':note}
    return render(request, 'lab4_index.html', response)

def note_list(request):
    note = Note.objects.all()
    response = {'Notes':note}
    return render(request, 'lab4_note_list.html', response)
    
def add_note(request):
    form = Noteform(request.POST or None)
    if request.method =='POST':
        if form.is_valid():
            data = form.cleaned_data
            x = 0
            for dat1 in data.values():
                x = x+int(dat1)
                
            if int(x) ==4:
                return HttpResponseRedirect('/lab-4/note-list')
            #if "1" in data.values():
               # return HttpResponseRedirect('/lab-4/note-list')
            return HttpResponseRedirect('/')
    
        
    response ={
            'form':form
        }
    return render(request,"lab4_form.html",response)