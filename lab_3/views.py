from django import forms
from django.shortcuts import redirect, render
from lab_1.models import Friend
from .forms import Friendform
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
# Create your views here.
@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'friend_list_lab3.html', response)
@login_required(login_url='/admin/login/')
def add_friend(request):
    form = Friendform(request.POST or None)
    if request.method =='POST':
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab-3/')
    
        
    response ={
            'form':form
        }
    return render(request,"lab3_form.html",response)
    