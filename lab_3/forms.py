from django import forms
from django.forms import fields
from lab_1.models import Friend

class Friendform(forms.ModelForm):
    class Meta:
        model = Friend
        fields=[
            'name',
            'NPM',
            'DOB'
        ]